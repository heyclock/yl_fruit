import Vue from 'vue'
import Router from 'vue-router'
import Empt from '@/components/Empt'

Vue.use(Router)

export default new Router({
  // https://blog.csdn.net/dq674362263/article/details/81876445
  mode: 'history',
  routes: [
    {
      path: '/empty.html/',
      name: 'Empt',
      component: Empt
    }
  ]
})
