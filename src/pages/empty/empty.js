// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Empty from './Empty.vue'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { Empty },
  template: '<Empty/>' // 可以修改为我们的页面App相关替换即可，或者在router里面配置路由
})
