// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Qs from 'qs'
import LyFruit from './LyFruit.vue'
import router from './router'
import VueJsonp from 'vue-jsonp'
import { Message } from 'element-ui'

// 网络请求axios - https://www.runoob.com/vue2/vuejs-ajax-axios.html
// import axios from 'axios'
// Qs是axios库中带的 - https://www.cnblogs.com/btgyoyo/p/6141480.html
// import Qs from 'qs'
import { post, fetch, patch, put } from '../../api/htttp.js'

import { jsonpfecth } from '../../api/jsonp.js'
Vue.prototype.$jsonpfecth = jsonpfecth

Vue.prototype.$post = post
Vue.prototype.$fetch = fetch
Vue.prototype.$patch = patch
Vue.prototype.$put = put
Vue.prototype.$Qs = Qs

Vue.config.productionTip = false

// VueJsonp
// https://www.cnblogs.com/rapale/p/7839203.html
Vue.use(VueJsonp, 5000)

// https://www.cnblogs.com/weizaiyes/p/7462274.html
// https://github.com/ElemeFE/element/issues/2024
// https://element.eleme.cn/#/zh-CN/component/quickstart
// Vue.use(Message)
Vue.prototype.$Message = Message

/* eslint-disable no-new */
export default new Vue({
  // el: '#app' 是lyfruit.html 的<div id="app"></div>
  el: '#app',
  router,
  // components 是声明有哪些组件
  components: { LyFruit },
  // template 是使用哪个组件
  template: '<LyFruit/>'
})
