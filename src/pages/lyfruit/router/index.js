import Vue from 'vue'
import Router from 'vue-router'
import FPic from '@/components/fruit/FPic'
import Empt from '@/components/Empt'

Vue.use(Router)

export default new Router({
  // https://blog.csdn.net/dq674362263/article/details/81876445
  mode: 'history',
  routes: [
    {
      path: '/lyfruit.html/',
      name: 'FPic',
      component: FPic
    },
    {
      path: '/empty.html/',
      name: 'Empt',
      component: Empt
    }
  ]
})
