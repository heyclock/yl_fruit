// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueJsonp from 'vue-jsonp'

// 网络请求axios - https://www.runoob.com/vue2/vuejs-ajax-axios.html
// import axios from 'axios'
// Qs是axios库中带的 - https://www.cnblogs.com/btgyoyo/p/6141480.html
// import Qs from 'qs'
import { post, fetch, patch, put } from './api/http'
Vue.prototype.$post = post
Vue.prototype.$fetch = fetch
Vue.prototype.$patch = patch
Vue.prototype.$put = put

// VueJsonp
// https://www.cnblogs.com/rapale/p/7839203.html
Vue.use(VueJsonp, 5000)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>' // 可以修改为我们的页面App相关替换即可，或者在router里面配置路由
})
