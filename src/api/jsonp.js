import Vue from 'vue'
// 引入App或者router的方式，进行路由
// 实现js进行路由
// import App from '../pages/lyfruit/lyfruit.js'
import router from '../pages/lyfruit/router'
import { Message, Loading } from 'element-ui'

let baseURL = 'http://120.27.112.29:8090'

// 创建一个进度条
// https://element.eleme.cn/#/zh-CN/component/loading
let loadingInstance = null

/**
 * 封装请求方法
 * @param url
 * @param params
 * @param callback
 */
export function jsonpfecth (url, params = {}, callback) {
  if (!loadingInstance) {
    loadingInstance = Loading.service({ fullscreen: true, text: '拼命加载中' })
  }
  Vue.jsonp(baseURL + url, params)
    .then(json => {
      if (callback) {
        callback(json)
      }
      if (loadingInstance) {
        loadingInstance.close()
      }
    }).catch(err => {
      if (typeof err.status !== 'undefined') {
        var message
        if (typeof err.statusText !== 'undefined') {
          message = err.statusText
        }
        switch (err.status) {
          case 404:
            break
          case 408:
            message = '请求超时!'
            break
        }
        Message(message)
      }
      // 找不到，跳转到火星页面
      if (loadingInstance) {
        loadingInstance.close()
      }
      // 导入lyfruit.js(App导入)，js中进行路由
      // 或者导入router进行路由
      // App.$router.push({ name: 'Empt' })
      router.push({ name: 'Empt' })
    })
}
