import axios from 'axios'
import qs from 'qs'
import { Message, Loading } from 'element-ui'

axios.defaults.timeout = 5000
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = '/api/'
} else {
  axios.defaults.baseURL = 'http://120.27.112.29:8090' // 'http://120.27.112.29:8090'
}

// 创建一个进度条
// https://element.eleme.cn/#/zh-CN/component/loading
let loadingInstance = null

// http request 拦截器
axios.interceptors.request.use(
  config => {
    loadingInstance = Loading.service({ fullscreen: true, text: '拼命加载中' })
    // const token = getCookie('名称');注意使用的时候需要引入cookie方法，推荐js-cookie
    // JSON.stringify(config.data) // '{"mobile":"xxx","password":xx}'
    config.data = qs.stringify(config.data) // 'mobile=xxx&password=xx'
    config.headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    // if(token){
    //   config.params = {'token':token}
    // }
    // 打印查看请求信息(包括请求的参数,url等)
    // console.log(config)
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

// http response 拦截器
axios.interceptors.response.use(
  response => {
    // 关闭进度条
    if (loadingInstance) {
      loadingInstance.close()
    }

    if (response.data.code === 1) {
      Message(response.data.message)
    } else if (response.data.code === 2) {
      // router.push({
      //   path:"/login",
      //   querry:{redirect:router.currentRoute.fullPath}//从哪个页面跳转
      // })
      Message('请重新登录')
    } else {
      return response.data
    }
    return Promise.reject(response)
  },
  err => {
    var es = JSON.parse(JSON.stringify(err))
    var errMsg
    if (typeof es.response !== 'undefined') {
      if (typeof es.response.status !== 'undefined') {
        errMsg = '' + es.response.status
      }
    } else {
      if (typeof es.code !== 'undefined') {
        errMsg = '' + es.code
      }
    }
    Message(errMsg)

    // 关闭进度条
    if (loadingInstance) {
      loadingInstance.close()
    }
    return Promise.reject(err)
  }
)

/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */
export function fetch (url, params = {}) {
  return new Promise((resolve, reject) => {
    axios.get(url, {
      params: params
    })
      .then(response => {
        console.log(response)
        resolve(response.data)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */
export function post (url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.post(url, data)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err)
      })
  })
}

/**
* 封装patch请求
* @param url
* @param data
* @returns {Promise}
*/
export function patch (url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.patch(url, data)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err)
      })
  })
}

/**
* 封装put请求
* @param url
* @param data
* @returns {Promise}
*/
export function put (url, data = {}) {
  return new Promise((resolve, reject) => {
    axios.put(url, data)
      .then(response => {
        resolve(response.data)
      }, err => {
        reject(err)
      })
  })
}
